/*
Why do we need to create our documents using semantic markup?
	- decouple things, loose components which brings the consequences we all
	want and know

div and span
	- don't provie any semantics
*/

// ============
// decouple the semantics and its styling
// ============
/*
old way
<div class="article">
	<div class="header">
		<h1>How I become a CSS Master</h1>
	</div>
</div>

- new way
<article>
	<header>
		<h1>How I become a CSS Master </h1>
	</header>
</article>

- we have improve the semantics  but with an unexpected side effect. 
- the only hooks for stlying are article and header elements, the css selectors
could look like 

article {
	...
}
article heder {
	...
}

- This css is not reusable. a better example would be

<article class="post">
	<header class="post-header">
		<h1>how I became a css master </h1>
	</header>
</article>

- then the css could be
.post{}
.post-header{}

*/
