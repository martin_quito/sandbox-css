/*
Why would I use css and ids?
	- just using semnatic html markup will not be enought to provide all the hooks
	to apply your style. You will need to use class and Id attributes

	- adds an interface to interact with the html. CSS can use it for stlying.
	Other technologies can use it to parse the document. The id and class
	don't inherhrenly add meaning to the tags. 

Difference betweeen id and class?
	- id can onl exist one per page and class multiple times
	
What name to give to classes?
	- don't give the name that describes how the element will look like, but
	a name that describes its current type.

Does classes and id add meaning to ur tags?
	- inherhrently, it doesn't

When to use ID and when to use class?
	- prefer classes only for styling and ids only to idenfiy elemetns to identify
	the leemnt for other purposes than stlying.
*/

// =============
// class and id
// =============
/*
<ul class="product-list"> // good name sintead of "large-cented-list"
	<li>...</li>
	<li>...</li>
	<li>...</li>
</ul>

<ul id="primary-product-list" class="product-list">/
	<li>...</li>
	<li>...</li>
	<li>...</li>
</ul>

- it can be assumed that thereis only one primary-product-list
so ID would be a good choice
- the id would enable to dd extra overriding styles 
- the id also would enable to add some interactiong with javascript
- in reality, ID is not a particuarly value as a hook for CSS. You'll usually
create more maintanable code when you use classes only for css and ID for other
purpose other than styling. 

*/