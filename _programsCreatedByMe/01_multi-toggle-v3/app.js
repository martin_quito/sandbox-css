Array.from(s('.nav li a')).forEach((n) => {
  let count = n.nextElementSibling ? 
    n.nextElementSibling.childElementCount : 0;
  if (count > 0) {
    addClass(n, 'parent');
  }
});

s('.toggleMenu').addEventListener('click', (e) => {
  e.preventDefault();
  let ele = e.currentTarget;
  toggleClass(ele, 'active');
  toggle(s('.nav'));
});

window.addEventListener('resize', adjustMenu);
window.addEventListener('orientationchange', adjustMenu);
adjustMenu();

function adjustMenu(e) {
  let vw = document.documentElement.clientWidth;
  let links = Array.from(s('.nav li'));
  let linksA = Array.from(s('.nav li a'));
  let linksP = Array.from(s('.nav li a.parent'));
  if (vw < 910) {
    show(s('.toggleMenu'), 'inline-block');
    if (!(hasClass(s('.toggleMenu'), 'active'))) {
      hide(s('.nav'));
    } else {
      show(s('.nav'));
    }
    links.forEach(e => {
        e.removeEventListener('mouseenter', hover);
        e.removeEventListener('mouseleave', hover);
    });

    linksP.forEach((n) => {
      n.addEventListener('click', click);      
    });

  } else {
    hide(s('.toggleMenu'));
    show(s('.nav'));

    links.forEach(e => {
      removeClass(e, 'hover');
    });

    linksA.forEach(e => {
      e.removeEventListener('click', click);
    });

    links.forEach(e => {
      e.addEventListener('mouseenter', hover);
      e.addEventListener('mouseleave', hover);      
    });
  }
}

// ===================
function hover(e) {
  toggleClass(e.currentTarget, 'hover');
}

function click(e) {
  e.preventDefault();
  toggleClass(e.currentTarget.parentNode, 'hover');
}


// ===================
function toggle(e) {
  if (e.style.display == 'none') {
    show(e);
  } else {
    hide(e);    
  }
}

function s(s) {
  let res = document.querySelectorAll(s);
  return res.length < 2 ? res[0] : res;
}

function show(e, type) {
  e.style.display = type || 'block';
}

function hide(e) {
  e.style.display = 'none';  
}

function removeClass(n, className) {
  let attr = n.getAttribute('class') || '';
  let pattern = new RegExp('\\b' + className + '\\b');  
  n.setAttribute('class', attr.replace(pattern, '').trim());
}

function addClass(n, className) {
  let clasi = n.getAttribute('class');
  if (clasi) {
    clasi = clasi + ' '  + className;
  } else { 
    clasi = className;
  }
  n.setAttribute('class', clasi);
}

function toggleClass(n, className) {
  let attr = n.getAttribute('class') || "";
  let pattern = new RegExp('\\b' + className + '\\b');
  if (attr.match(pattern)) {
    n.setAttribute('class', attr.replace(pattern, '').trim());
  } else {
    addClass(n, className);
  }
}

function hasClass(n, className) {
  let attr = n.getAttribute('class');
  let pattern = new RegExp('\\b' + className + '\\b');
  if (attr.match(pattern)) {
      return true;
  } else {
      return false;
  }
}
