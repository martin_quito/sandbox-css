/*
What are the benefits of using the new 'svg with js'?
    - the icons are in svg
    - the svg icons inherit css SIZE and COLOR
    - icons will load async which means it will not block
        the page from rendering. 
    - power transforms
    - masking
    - layering text and counters
    - auto-accessability

How are they package?
    - SVG and JS
    - web fonts and CSS
    - SVG sprites
    - raw SVGS

What are some disadvantages of 'svg with js'?
    - because the icons are loaded async, it can
    cause the layout to shift

What are the styles?
    - fas (fa is for backward compability)
        - solid
        - availability: free

    - far (pro)
        - regular
        - availability: pro

    - fal (pro)
        - light
        - availability: pro

    - fab
        - brands
        - availability: free

What elements to use with font-awesome icons?
    - i
    - em
    - span

What to know when referencing an icon?
    - style-prefix
    - the style you want to use's corresponding prefix

Old versions of font awesome?
    - fa prefix has been deprecated in version 5
*/

