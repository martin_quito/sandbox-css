/*
What is cascade?
	- helps to resolve the problem when two or more rules target the same
	element. It works by assining "importance" to each rule

	- cascade importances
		User styles flagged as !important
		Author styles flagged as !important
		Author styles
		User styles
		Styles applied by the browser/user agent

*/