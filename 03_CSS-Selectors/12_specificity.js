/*
What?
	- how specify the css rule is
		- a css rule has a specificity which needs to be calculated
			- each css SELECTOR has a numeric value
		- the specificity of a css rule is broken down intwo 4  constituent
		levels a,b,c,d
			a - if the style is an inline style, equals 1
			b - b equals the total number of ID selectors
			c - c equals the total number of class, pseudo, and attribute 
				selectors
			d - d equals the total number of type selectors and pseudo element
				selectors
	- rules with more specifiy selectors override those with less
	specific ones

Why?
	- specificity allows to set css rules for genereal elements
	and then override them for more specific elemenets

Example
Style=""					1,0,0,0			1000
#wrapper #contnet {}		0,2,0,0			200
#content .datePosted{}		0,1,1,0			110
div#content {}				0,1,0,1			101
#content {}					0,1,0,0			100
p.comment.datePosted{}		0,0,2,1			21
p.comment{}					0,0,1,1			11
div p {}					0,0,0,2			2
p {}						0,0,0,1			1
*											0

What happens if the two rules have the same specificty?
	- the last one takes precedent
		- this says that I need to CONSIDER the order of the css rules (or
			css slectors)
		

*/
