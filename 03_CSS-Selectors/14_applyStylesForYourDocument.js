/*
How to apply css to your document?
	- styles directly to the head of a document by placing them in a 'style'
	element
		- small size of styles that you want to apply immediately
		and not overload of the browser downloading a separate file
	- external style sheet so it can be reusability purposes
		- goood for reuse for different pages
		- good for reuse for different sites
	- @import directive to load an external file
		- where
			- use in the style block in the head of your html doc
			- or in the external life itself

*/