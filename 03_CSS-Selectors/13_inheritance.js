/*
Why? 
  - to not duplicate css rules
  - can help to simplify css

Problems
  - determining where the styles originate can become confusing

What
  - descendnates inherite css rules 

How?
	- inherited property values have no specificity at all, not even zero
 
Example
	- color and font size are inherited by the descendatns
*/