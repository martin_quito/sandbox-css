/*
Data Tables
    - a 'caption' element inside a table acts as a heading for a table
    - 'tfoot' 
    - 'tbody'
    - 'thead'
    - you can only use one 'tfoot' and 'thead' per table and as many 'tbody' for complex
    tables
    - row and column headings should be marked up with 'th' rather than 'td'
        - table headings have an attribute 'scope' to describe if the 'th' is a row heading
        or a column heading. Possibel values are
            - scope='col'
            - scope='row'
            - scope='rowgroup'
            - scope='colgroupw'
    - 'tr', table rows
    - 'td' table cells

    - 'colgroups' element
        - used to style the columns
        - colgroup is used to define a group one or more columns, represented by the col element
        - colgroup needs to be placed inside the 'table' element, after any 'caption' but before any 'thead',
        'tbody' or 'tfoot'
        - the properties that can be used to columns are severely limited
            - background properties
            - border properties
            - width
            - and visibility (not well supported by browsers)
                - only values of 'visible' and 'collapse'
        - example
            <colgroup>
                <col class='cal-mon'>
                <col class='cal-tue'>        
                <col class='cal-wed'>
                <col class='cal-thr'>                
                <col class='cal-fri'>                
                <col class='cal-sat cal-weekend'>
                <col class='cal-sun cal-weekend'>        
            </colgroup>
    
    - 'border-collapse'
        values
            - collapse
                - cells share borders
            - separate
                - borders are placed around invidiual cells
        what?
            - border of the cells
    - 'table-layout'
        - what?
            - control the sizing algorithm, it sets the 'table' and 'column' widths
        - values
            - auto
                - browser decides the width of the cells based on their content
            - fixed
                - uses the widths of 'table' and 'col' elements
                - OR uses the width of the cells in the first row of the table
Forms

*/