/*
What is progressive enhancement?
	- see book

What is graceful degradation? (aka Faul tolerance)
	- failt tolerance: a property that enables a system to work propetyly in 
	an event of a failure of some of its componenets

	- there are different types of fault tolerance, failin gracefully, fail soft (fail safe)
	, fail deadly, etc. The one 

	- fail soft (graceful degradation)
		- for example, insufficuent bandwidth is available to stram an onlkine
		video, a lower-resolution might be streamed in place of a higher resolution
	- see beloe

- the environemnt we web developers work in is tricky because we don't know
the devices users will use to consume our content, period. We have content to
deliver to users and there is a probablity the end-users devices will render
our content incorrectly because the content is wrapped with technology that 
is not fully, maybe partyially supported by the end-users devices. We cannot
control their updates and their os. Therefore there are some philosophies that
can help us to achieve this. 
	- graceful degration
		- you first build your web functionality so that it provides a level
		of user experience in most modern browsrs, but it will also
		degrade gracefully to a lower user expeirence in older brownsers.
		In other wods, you build something and then make it barrely work
		in other environemnts or asking users to upgrade. 
		- make a web page avaialbe to a varietty of browser by develooping a 
		baseline FULL of features and then removing layeers of features to 
		less capable browsers
		- examples
			- a 24-bit png transpare image works in modern browsers but in 
			older borwsers, it could just render a broken image or text
			- often specify browser level support
				- level 1 browsrs (best experience)
				- level 2 (degrated experience)

	- regressive enhancement
		- make a web page availabe to a variety of browsers by developing
		from a basefile of full features, and then replicating those full
		features to less capable browsers

	- progressive enhacnement
		- you first estlbiahs a basic level of experience that all borwsers will
		be able to provide when rendering the site, but also build in more
		advance functionaity that will automatically be availabe to browsers that 
		can use it

		- make web page available to a varitety of browsers by develping
		a baseline of COMPATIBLE and then layeting enhanched features to more
		capable browsers

		- in other words, you write codes in layers, you start with code
		that will work with all browsers, and then each successive enhancement
		is only applied if is supported.  

		- examples
			- you don't choose browsers but technology, it needs to support
			XHR or indexDB, etc and work from there. 

	- example	
		you have an anchor to print with JS and you need it to work in browser where JS
		is disabled too. 
			- graceful degration - you will add a note to the user to
			enable javascript.  Or perphas tell the user how to print from the
			browser. This is bad because we assuming a lot of things that could
			be wrong
			- progressinve enhacmenet - the first step is to find out
			if there is a non-scripting way of printing a page. well there isin't
			which means that the anchor link is a wrong choice of HMTL elemtnt.
			Second, not asusme that the user has JS enabled and that browser can
			print. Instead, put a gneric text to tell them to print and leave
			the 'how' up to them. And then, add code to show a link if the
			browser has JS enabvled and supports the print function. 

Why vendor prefixes?
	- browsers append css propeties if they are experiemental features so
	that only their browser can interpret it. The problem is that
	other borwsers mimic others borwsers by reading properties meant for
	other borwsers. As a consequenec, most browser are turning away
	from vendor prefixes and moving twoards enabling experiementa features
	using preference fagls or preview releases. 

Conditional rules and detection scripts?
	- css has a conditional rule
		@supports(display grid) {...}

		The problem is that some old borwsers amy not understan d it
	- we can use js is something is supported or not


Reference
1. Graceful degration vs progressinve enchamnet
https://www.w3.org/wiki/Graceful_degradation_versus_progressive_enhancement

*/
// =============
// vendor prefixes
// =============
/*
.myThing {
	-webkit-transofrm:...
	-moz-transofmr: ...
	-ms-transform:...
	transform:...
}
*/

// =============
// CSS progressinve enhancement
// =============
/*
.overlay {
	background-color #000;  // will be applied to all browsers
	background-color: rgba(0,0,0,0.8) // old borwsers will ignore this and new browser swill implement it
}
*/

// =============
// html progessive enhacement
// =============
// browser will ignore elements or use a default


// =============
// CSS progressinve enhancement
// =============
/*
.overlay {
	background-color #000;  // will be applied to all browsers
	background-color: rgba(0,0,0,0.8) // old borwsers will ignore this and new browser swill implement it
}
*/

// =============
// html progessive enhacement
// =============
// browser will ignore elements or use a default
