/*
What is the difference betwene layout and structure and design?
	- design includes the structure and layout ???
	- structure refers to the order of the elements ???
	- layout is the blueprint where things shoauld go ???

What happen to the HTML?
	- in 1990 Tim Bernes Lee created HTML. HTML gives strcture to dcoument
	and meaning. As HTML became popular, style was needed. Develoeprs started
	to use HTML for layout purposes such as the table and other tags too. 
	This kind of use is called 'tag soup'. The web became a mess and
	css was created to help tidy things up. 

What is the primary purpose of CSS?
	- extract the presentation rules in HTML and put it into their own system;
	to separate presentation and content. This would mean that tables
	and font atags be ditched.

What does CSS provide?
	- only to control visual and layout (style)
	- reuse styles
	- improved code structured through separation of concerns

History of CSS?
	- W3C (Word Wide Web Consortium) is in charge in standarazing CSS
	- 1996
		- CSS1 standard
			- basic stying
	- ?	
		- CSS2 Standard 
			- adds position, floating
			- it needed 9 years from Aug 2002 to June 2011 to reach Recommendation 
			status
	- 2002
		- CSS 2.1 Standard
	- ? 
		- CSS 3 Standard
			- there is no CSS 3 per se but a collection of modules. Each module
			is independent part of the langauge moves towards standardziong as its
			own pace. 
			- when a module improves an exiting concept of CSS, it starts as level 3
			, it is a entire new concenpt/technoclogy, it starts a level 1

History of HTML?
	- 1999 HTMl 4.1 and W3C switched its attention to XHTML 1.0 but it fell
	throuhg. Essential the grow of the HTML stalled. So in 2004, a new group
	was form (WHATWG) to create a new set of specification for HTML. They did this
	because of the slow develp of W3C and its decision to abondom HTML in
	favor of XML based technologies. W3C joined in but them later separated.
	Today there are 2 specifications, HTML 5 (from W3C) and HTML (from W3C).
	
What versions of HTML and CSS should I use?
	- it is not an easy answer, instead you should ask what components
	of HMTL and CSS have been implemented in browsers and how robust, bug-free
	those implemenetnatiosn are?
	- one of the trickiest thing of writing css and html is understanding
	the state of "browser support"
*/
